/*jshint esversion: 6 */
let canv = document.querySelector("#canvas");
let ctx = canv.getContext("2d");
let playerToken = "X";
let gameState = false;
let movesCount = 0;
let winMove = -1;
let board = [["0", "0", "0"], ["0", "0", "0"], ["0", "0", "0"]];
let autoGameDelay = 500;
let betChoose = "";
let balance = 2337;
let betAmount = 0;
let gamesCount = 0;
let k = 2; // Коэфф ставки
let gameHistory = [];                                               //TODO

let winIndex = [
  [[0, 0], [1, 0], [2, 0]],
  [[0, 1], [1, 1], [2, 1]],
  [[0, 2], [1, 2], [2, 2]],
  [[0, 0], [0, 1], [0, 2]],
  [[1, 0], [1, 1], [1, 2]],
  [[2, 0], [2, 1], [2, 2]],
  [[0, 0], [1, 1], [2, 2]],
  [[2, 0], [1, 1], [0, 2]]
];

canv.width = 600;
canv.height = 600;

function drawBoardLines() {
  drawLine(600, 200, "left");
  drawLine(200, 0, "down");
  drawLine(0, 400, "right");
  drawLine(400, 600, "up");
}

function drawLine(x, y, direction) {
  let len = 0;
  setTimeout(function() {
    let timer = setInterval(function() {
      requestAnimationFrame(function() {
        ctx.strokeStyle = "#30336b";
        ctx.lineWidth = 8;
        ctx.beginPath();
        if (direction == "left") {
          ctx.moveTo(x - len, y);
          ctx.lineTo(x - len - 5, y);
        }

        if (direction == "down") {
          ctx.moveTo(x, y + len);
          ctx.lineTo(x, y + len + 5);
        }

        if (direction == "right") {
          ctx.moveTo(x + 5, y);
          ctx.lineTo(x + len + 5, y);
        }

        if (direction == "up") {
          ctx.moveTo(x, y - len);
          ctx.lineTo(x, y - len - 5);
        }

        len = len + 5;
        ctx.stroke();
        if (len >= 600) {
          clearInterval(timer);
        }
      });
    }, 4);
  }, 500);
}

function drawCross(i, j) {
  let x,
    y = 0;
  x = 50 + 200 * j;
  y = 50 + 200 * i;

  let len = 0;
  let timer = setInterval(function() {
    ctx.strokeStyle = "#686de0";
    ctx.lineWidth = 8;

    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x + len, y + len);
    ctx.stroke();

    len = len + 5;
    if (len >= 105) {
      clearInterval(timer);
    }
  }, 5);

  setTimeout(function() {
    let lenY = 0;
    let lenX = 100;
    let timer = setInterval(function() {
      ctx.strokeStyle = "#686de0";
      ctx.lineWidth = 8;

      ctx.beginPath();
      ctx.moveTo(x + 100, y);
      ctx.lineTo(x + lenX, y + lenY);
      ctx.stroke();

      lenY = lenY + 5;
      lenX = lenX - 5;
      if (lenY >= 105) {
        clearInterval(timer);
      }
    }, 5);
  }, 200);
}

function drawCircle(i, j) {
  let x,
    y = 0;
  x = 100 + 200 * j;
  y = 100 + 200 * i;

  let len = 0;
  let timer = setInterval(function() {
    ctx.strokeStyle = "#e056fd";
    ctx.lineWidth = 8;
    ctx.beginPath();
    ctx.arc(x, y, 50, 0, Math.PI * len);
    ctx.stroke();
    len = len + 0.05;
    if (len >= 2.5) {
      clearInterval(timer);
    }
  }, 8);
}

function makeMove(i, j) {
  if (gameState) {
    if (board[i][j] == 0) {
      if (playerToken == "X") {
        drawCross(i, j);
        board[i][j] = 1;
        movesCount++;
        winner();
        playerToken = "O";
        return;
      }
      if (playerToken == "O") {
        drawCircle(i, j);
        board[i][j] = -1;
        movesCount++;
        winner();
        playerToken = "X";
        return;
      }
    }
  }
}

function isTie() {
  if (movesCount > 8) {
    return true;
  }
  return false;
}

function winner() {
  if (checkWinner()) {
    winLineDraw(winMove);
    gameState = false;
    if (playerToken == betChoose) {
      betResult("win");
    } else {
      betResult("lose");
    }
  } else if (isTie()) {
    gameState = false;
    if (betChoose == "TIE") {
      betResult("win");
    } else {
      betResult("lose");
    }
  }
}

function checkWinner() {
  for (let c = 0; c < 8; c++) {
    let cond = true;
    for (let n = 0; n < 2; n++) {
      if (board[winIndex[c][n][1]][winIndex[c][n][0]] != "0") {
        if (
          board[winIndex[c][n][1]][winIndex[c][n][0]] ==
            board[winIndex[c][n + 1][1]][winIndex[c][n + 1][0]] &&
          cond == true
        ) {
          cond = true;
          winMove = c;
        } else {
          cond = false;
        }
      } else {
        cond = false;
      }
    }
    if (cond) {
      return true;
    }
  }
  return false;
}

function winLineDraw(c) {
  let color = "rgba(235,77,75,0.5)";
  let lineWidth = 15;
  let offset = 5;
  let delay = 5;
  let lineLenght = 575;
  let timeOut = 700;
  setTimeout(function() {
    if (c <= 2 && c != -1) {
      let len = 25;
      let timer = setInterval(function() {
        let cY = 100 + winIndex[c][0][1] * 200;
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;

        ctx.beginPath();
        ctx.moveTo(len, cY);
        len = len + offset;
        ctx.lineTo(len, cY);
        ctx.stroke();

        if (len >= lineLenght) {
          clearInterval(timer);
        }
      }, delay);
    }
    if (c <= 5 && c >= 3) {
      let len = 25;
      let timer = setInterval(function() {
        let cX = 100 + winIndex[c][0][0] * 200;
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;

        ctx.beginPath();
        ctx.moveTo(cX, len);
        len = len + offset;
        ctx.lineTo(cX, len);
        ctx.stroke();

        if (len >= lineLenght) {
          clearInterval(timer);
        }
      }, delay);
    }
    if (c == 6) {
      let len = 25;
      let timer = setInterval(function() {
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;

        ctx.beginPath();
        ctx.moveTo(len, len);
        len = len + offset;
        ctx.lineTo(len + 0.1, len + 0.1);
        ctx.stroke();

        if (len >= lineLenght) {
          clearInterval(timer);
        }
      }, delay);
    }
    if (c == 7) {
      let lX = 575;
      let lY = 25;
      let timer = setInterval(function() {
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;

        ctx.beginPath();
        ctx.moveTo(lX, lY);
        lX = lX - offset;
        lY = lY + offset;
        ctx.lineTo(lX - 0.1, lY + 0.1);
        ctx.stroke();

        if (lY >= lineLenght) {
          clearInterval(timer);
        }
      }, delay);
    }
  }, timeOut);
}

function betResult(result) {
  let resultText = "";
  if (result == "win") {
    balance = balance + betAmount * k;
    drawBalance();
    resultText =
    "You bet win!<span class=" +
    "colorGreen" +
    ">" +
    " +" +
    betAmount * k +
    "</span>";
  }
  
  if (result == "lose") {
    drawBalance();
    resultText =
    "You bet lose!<span class=" +
      "colorRed" +
      ">" +
      " -" +
      betAmount +
      "</span>";
  }
  gamesCount = gamesCount + 1;
  gameHistory.push([gamesCount, betChoose, betAmount, result]);
  drawResultText(resultText);
}
  
function randomInteger(min, max) {
  // случайное число от min до (max+1)
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

function autoGame() {
  disableButtons();
  balance = balance - betAmount;
  drawBalance();
  drawResultText("Bets accepted.");
  // random start token
  if (randomInteger(1,2) == 1) {
    playerToken = "O";
  } else {
    playerToken = "X";
  }
  if (gamesCount >= 1) {
    restart();
    setTimeout(function() {
      gameState = true;  //! gameState внутри ифоф т.к нужен запуск с задержкой для анимаций
      autoPlay();
    }, 1500);
  } else {
    gameState = true;
    autoPlay();
  }
}

function autoPlay() {
  let timer = setInterval(function() {
    if (!gameState) {
      clearInterval(timer);
      enableButtons();
      return;
    }
    let i = randomInteger(0, 2);
    let j = randomInteger(0, 2);

    while (board[i][j] != "0") {
      i = randomInteger(0, 2);
      j = randomInteger(0, 2);
    }

    makeMove(i, j);
  }, autoGameDelay);
}

function clearCanvas() {
  let len = 0;
  let timer = setInterval(function() {
    ctx.fillStyle = "rgba(255,255,255, 0.1)";
    ctx.beginPath();
    ctx.fillRect(0, 0, 600, 600);
    ctx.stroke();
    len = len + 1;
    if (len >= 50) {
      clearInterval(timer);
    }
  }, 30);
}

function restart() {
    clearCanvas();
    movesCount = 0;
    winMove = -1;
    board = [["0", "0", "0"], ["0", "0", "0"], ["0", "0", "0"]];
    setTimeout(function() {
      drawBoardLines();
    }, 900);
}

function disableButtons() {
  document.querySelector("#noughts").onclick = null;
  document.querySelector("#crosses").onclick = null;
  document.querySelector("#tie").onclick = null;
  document.querySelector("#betAmount").onchange = null;
}

function enableButtons() {
  document.querySelector("#noughts").onclick = function(e) {
    if (!gameState) {
      autoGame();
      k = 2;
      betChoose = "O";
    }
  };
  
  document.querySelector("#crosses").onclick = function(e) {
    if (!gameState) {
      autoGame();
      k = 2;
      betChoose = "X";
    }
  };
  
  document.querySelector("#tie").onclick = function(e) {
    if (!gameState) {
      autoGame();
      k = 6;
      betChoose = "TIE";
    }
  };

  document.querySelector("#betAmount").onchange = function() {
    betAmount = document.querySelector("#betAmount").value;
    document.querySelector("#betRangeText").innerHTML =
      "Bet amount: " + betAmount + "$";
  };
}

function drawBalance() {
  document.querySelector("#infoText").innerHTML = "balance: $" + balance;
  setTimeout(function(){
    betAmount = document.querySelector("#betAmount").value;
    document.querySelector("#betRangeText").innerHTML =
      "Bet amount: " + betAmount + "$";
  }, 100);
}

function drawResultText(text) {
  document.querySelector("#resultText").innerHTML = text;
}

function init() {
  drawBoardLines();
  drawBalance();
  enableButtons();
}

init();